# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 0.1.2 - 2020-06-17

### Added

- Automatic release creation in CI pipeline

## 0.1.1 - 2020-06-16

### Added

- Continuous publication to npm registry

## 0.1.0 - 2020-06-16

### Added

- First public version
- `LoadOver` component displays a loading overlay on top of its children
- `LoadInto` component displays a loading animation inside its single child
