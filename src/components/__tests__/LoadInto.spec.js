import { mount } from '@vue/test-utils';

import LoadInto from '../LoadInto';

describe('LoadInto', () => {
  test('matches snapshot', () => {
    const loadInto = mount(LoadInto, {
      slots: {
        default: '<div>Hello World!</div>'
      },
      propsData: {
        loading: false
      }
    });
    expect(loadInto).toMatchSnapshot();
  });

  test('matches snapshot (loading)', () => {
    const loadInto = mount(LoadInto, {
      slots: {
        default: '<div>Hello World!</div>'
      },
      propsData: {
        loading: true
      }
    });
    expect(loadInto).toMatchSnapshot();
  });
});
