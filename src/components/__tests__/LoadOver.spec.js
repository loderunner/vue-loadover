import { mount } from '@vue/test-utils';

import LoadOver from '../LoadOver';

describe('LoadOver', () => {
  test('matches snapshot', () => {
    const loadOver = mount(LoadOver, {
      slots: {
        default: '<div>Hello World!</div>'
      },
      propsData: {
        loading: false
      }
    });
    expect(loadOver).toMatchSnapshot();
  });

  test('matches snapshot (loading)', () => {
    const loadOver = mount(LoadOver, {
      slots: {
        default: '<div>Hello World!</div>'
      },
      propsData: {
        loading: true
      }
    });
    expect(loadOver).toMatchSnapshot();
  });
});
