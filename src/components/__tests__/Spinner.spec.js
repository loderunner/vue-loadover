import { mount } from '@vue/test-utils';

import Spinner from '../Spinner';

describe('Spinner', () => {
  test('matches snapshot', () => {
    const spinner = mount(Spinner);
    expect(spinner).toMatchSnapshot();
  });
});
