# vue-loadover

vue-loadover is a collection of components for Vue.js that provide loading state inside your templates. Wrap any template node that should have a loading state in one of the vue-loadover components.

## Quick start

#### 1. Install `vue-loadover`

```shell
npm install vue-loadover
```

#### 2. Use in your templates

Display a full-size loading overlay on top of your component, controlled by the `loading` data prop.

```vue
<template>
  <load-over :loading="loading">
    <div>Hello World!</div>
  </load-over>
</template>

<script>
import { LoadOver } from 'vue-loadover';

export default {
  components: {
    LoadOver
  },
  data() {
    return { loading: true };
  },
  mounted() {
    // Display loading screen for 5 seconds
    setTimeout(() => (this.loading = false), 5000);
  }
};
</script>
```

> See the [`examples`](/examples) for a deeper look on how to use LoadOver and other components.

## Detailed documentation

### Installation

vue-loadover is available as a public package from the npm registry. Install using your project's package manager.

```shell
npm install vue-loadover
# Or, using yarn
yarn add vue-loadover
```

### Usage

vue-loadover components are designed to _wrap_ the DOM nodes in your templates providing an easy mechanism to display a loading state on your components.

All vue-loadover components have a `loading` prop that can be set to control the state of the loading component.

##### Props

| Name      | Type      | Required           | Default |
| --------- | --------- | ------------------ | ------- |
| `loading` | `Boolean` | :white_check_mark: |         |

#### LoadOver

The `LoadOver` component displays a semi-transparent overlay with an animated spinner in the center when `loading` is `true`.

```vue
<template>
  <load-over :loading="loading">
    Hello World!
  </load-over>
</template>
```

`LoadOver` uses [slots](https://vuejs.org/v2/guide/components-slots.html) to overlay a white `<div>` on top of its children nodes. The animated spinner will scale with its content to a maximum size of 64x64 pixels.

> :warning: The overlay is an absolutely positioned sibling of your content. Be careful when designing your layout.

#### LoadInto

The `LoadInto` component injects an animated spinner in the center of the its child node, displayed when `loading` is `true`.

```vue
<template>
  <main>
    <load-into :loading="loading">
      <span>Hello World!</span>
    </load-into>
  </main>
</template>
```

The spinner is inside an absolutely positioned `<div>` that is injected inside your component. When `loading` is `true`, `LoadInto` hides all other children and displays the spinner animation.

> :warning: Because the spinner animation is injected directly inside your component, `LoadInto` _must_ have a single (non-text) child node. Using a text node or more than one node will result in undefined behavior.

## Contributing

If you are interested in contributing to vue-loadover feel free to fork the repository and submit a merge request into the `dev` branch.

Here are a few ideas of things that could be added:

##### TODO

- Unit tests
- Add a slot for custom loader
- Add spinner SVG designs and animations (pure CSS loaders?)
- Props to configure `LoadOver` overlay color and opacity
- Props to configure SVG content color and opacity
- Documentation website
  - Use [VuePress](https://vuepress.vuejs.org/)
  - Deploy to GitLab Pages

## License

vue-loadover is released under the [Apache 2.0 License](LICENSE).

Copyright (c) 2020 Charles Francoise
