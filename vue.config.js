module.exports = {
  productionSourceMap: false,
  css: {
    sourceMap: process.env.NODE_ENV === 'development'
  },
  chainWebpack: config => {
    if (process.env.NODE_ENV === 'development') {
      config.devtool('eval-source-map');
    }

    const svgRule = config.module.rule('svg');
    svgRule.uses.clear();
    svgRule
      .rule('svg')
      .use('babel-loader')
      .loader('babel-loader')
      .end()
      .use('vue-svg-loader')
      .loader('vue-svg-loader');
  }
};
