### Problem description

(Describe the problem, including screenshots or code snippets if available)

### Environment

#### Versions

- Vue-overload:
- Vue:

#### Local environment

- Operating system:
- Browser and version:

### Reproduction

1. Describe the steps to reproduce the issue
2. Including preliminary setup
3. And any relevant information

#### Expected behavior

(What did you expect to see?)

#### Observed behavior

(What did you see instead?)
