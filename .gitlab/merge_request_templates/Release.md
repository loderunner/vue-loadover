## Describe the release

## Does this MR meet the acceptance criteria?

- [ ] `README.md` is up-to-date
- [ ] `CHANGELOG.md` is up-to-date
- [ ] Run `npm run bump`
