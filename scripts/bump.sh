#!/usr/bin/env sh

set -e

echo 'Fetching remote...'
git fetch origin
echo

declare res

while [ "x$res" != 'xn' ]; do
    printf '\033[0;33mMAJOR\033[0m Does this release contain backward-incompatible API changes? ([y]es/[n]o/show [d]iff) '
    read res

    if [ "x$res" = 'xd' ]; then
        git diff origin/main
    elif [ "x$res" = 'xy' ]; then
        new_version=$(npm --no-git-tag-version version major)
        echo "Bumped version to ${new_version:1}"
        exit 0
    elif [ "x$res" != 'xn' ]; then
        echo 'Invalid input - please enter one of y/n/d'
    fi
done

res=""

while [ "x$res" != 'xn' ]; do
    printf '\033[0;33mMINOR\033[0m Does this release introduce new features in a backward-compatible way? ([y]es/[n]o/show [d]iff) '
    read res

    if [ "x$res" = 'xd' ]; then
        git diff origin/main
    elif [ "x$res" = 'xy' ]; then
        new_version=$(npm --no-git-tag-version version minor)
        echo "Bumped version to ${new_version:1}"
        exit 0
    elif [ "x$res" != 'xn' ]; then
        echo 'Invalid input - please enter one of y/n/d'
    fi
done

res=""

while [ "x$res" != 'xn' ]; do
    printf '\033[0;33mPATCH\033[0m Does this release contain backward-compatible bugfixes or improvements? ([y]es/[n]o/show [d]iff) '
    read res

    if [ "x$res" = 'xd' ]; then
        git diff origin/main
    elif [ "x$res" = 'xy' ]; then
        new_version=$(npm --no-git-tag-version version patch)
        echo "Bumped version to ${new_version:1}"
        exit 0
    elif [ "x$res" != 'xn' ]; then
        echo 'Invalid input - please enter one of y/n/d'
    fi
done

echo
echo 'Unknown changes. Could not bump version.'
exit 1